<?php

use Dcat\Admin\Admin;
use Dcat\Admin\Grid;
use Dcat\Admin\Form;
use Dcat\Admin\Grid\Filter;
use Dcat\Admin\Show;

/**
 * Dcat-admin - admin builder based on Laravel.
 * @author jqh <https://github.com/jqhph>
 *
 * Bootstraper for Admin.
 *
 * Here you can remove builtin form field:
 *
 * extend custom field:
 * Dcat\Admin\Form::extend('php', PHPEditor::class);
 * Dcat\Admin\Grid\Column::extend('php', PHPEditor::class);
 * Dcat\Admin\Grid\Filter::extend('php', PHPEditor::class);
 *
 * Or require js and css assets:
 * Admin::css('/packages/prettydocs/css/styles.css');
 * Admin::js('/packages/prettydocs/js/main.js');
 *
 */
Admin::css('/css/admin.css?version=2.0.0');

// 表格初始化
Grid::resolving(function (Grid $grid) {
    $grid->model()->orderByDesc('id'); // 默认排序规则
    $grid->addTableClass(['table-text-center']); // 设置表格文字居中
    $grid->disableRowSelector(); // 禁用行选择按钮

    $grid->tools(function ($tools) {
        $tools->batch(function ($batch) {
            $batch->disableDelete(); // 禁用批量删除
        });
    });
    $grid->filter(function ($filter) {
        // 禁止自动展开过滤器
        $filter->expand(false);
    });
});

// 表单初始化
Form::resolving(function (Form $form) {
    $form->disableDeleteButton(); // 禁用删除按钮
    // 禁用Check按钮
    $form->disableEditingCheck();
    $form->disableViewCheck();
    $form->disableCreatingCheck();
    $form->disableResetButton(); // 禁用重置按钮
    $form->disableViewButton(); // 禁用显示详情按钮
});

// 详情初始化
Show::resolving(function (Show $show) {
    $show->disableEditButton(); // 禁用编辑按钮
    $show->disableDeleteButton(); // 禁用删除按钮
});
