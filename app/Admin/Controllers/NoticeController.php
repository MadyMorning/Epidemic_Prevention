<?php

namespace App\Admin\Controllers;

use Dcat\Admin\Admin;
use Dcat\Admin\Form;
use Dcat\Admin\Grid;
use Dcat\Admin\Layout\Content;
use Dcat\Admin\Http\Controllers\AdminController;

use App\Models\Notice;

class NoticeController extends AdminController
{
    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Grid::make(new Notice(), function (Grid $grid) {
            $grid->column('title');
            $grid->column('img_url')->image('', '', 40);
            $grid->column('start_time');
            $grid->column('end_time');
            $grid->column('file_url')->display(function ($value) {
                return $value ? '<a href="' . \App\Models\Notice::getFile($value) . '" target="_blank" download >' . basename($value) . '</a>' : '';
            })->width(100);
            $grid->column('created_at');
            $grid->filter(function (Grid\Filter $filter) {
                $filter->panel();

                $filter->like('title')->width(2);
                $filter->date('start_time')->width(3);
                $filter->date('end_time')->width(3);
            });
        });
    }

    public function show($id, Content $content)
    {
        return parent::show($id, $content->breadcrumb([
            'text' => '公告通知列表', 'url' => '/notice'
        ], ['text' => '']));
    }

    /**
     * 详情页
     */
    protected function detail($id)
    {
        admin_style('.col-md-12 {
        background: #fff;
        padding:1rem;
    }');
        // 查询字段
        $fields = ['id', 'title', 'img_url', 'file_url', 'content', 'start_time', 'end_time', 'admin_id', 'created_at', 'updated_at'];
        $notice = Notice::select($fields)->find($id);
        // 默认路径
        $list = [
            admin_trans_field('title') => $notice->title,
            admin_trans_field('content') => $notice->content,
            admin_trans_field('start_time') => $notice->start_time,
            admin_trans_field('file_url') => $notice->file_url ? "<a href='" . Notice::getFile($notice->file_url) . "' target='_blank' download='{$notice->title}'>$notice->title</a><br>" : '',
        ];

        return admin_view('detail', ['list' => $list]);
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Form::make(new Notice(), function (Form $form) {
            $form->text('title')->required();
            $form->image('img_url')->move('notice/images')->uniqueName()->autoUpload()->retainable()->help('上传图片大小:919*258,格式为png,jpg都可')->required();
            $form->editor('content')->required();
            $form->file('file_url')->move('notice/files')->uniqueName()->autoUpload()->retainable();
            $form->date('start_time');
            $form->date('end_time');
            $form->hidden('admin_id');

            $form->saving(function (Form $form) {
                if ($form->isCreating()) {
                    $form->input('admin_id', Admin::user()->id);
                }
            });
        });
    }
}
