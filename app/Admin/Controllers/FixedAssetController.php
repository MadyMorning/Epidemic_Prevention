<?php

namespace App\Admin\Controllers;

use App\Models\FixedAsset;
use App\Models\FixedAssetType;
use Dcat\Admin\Admin;
use Dcat\Admin\Form;
use Dcat\Admin\Grid;
use Dcat\Admin\Show;
use Dcat\Admin\Http\Controllers\AdminController;

class FixedAssetController extends AdminController
{
    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Grid::make(FixedAsset::with(['adminUser:id,name', 'fixedAssetType:id,name']), function (Grid $grid) {
            $grid->disableDeleteButton(); // 禁用删除按钮
            $grid->disableRowSelector(false);

            $grid->column('id')->sortable();
            $grid->column('adminUser.name');
            $grid->column('asset_number');
            $grid->column('asset_name');
            $grid->column('asset_spec');
            $grid->column('price');
            $grid->column('fixedAssetType.name');
            $grid->column('amount');
            $grid->column('amount_idle');
            $grid->column('amount_using');
            $grid->column('explain');
            $grid->column('inventory_date');

            $grid->filter(function (Grid\Filter $filter) {
                $filter->panel();

                $filter->like('asset_name')->width(2);
                $filter->where('fixedAssetType.name', function ($query) {
                    $query->where('asset_type', $this->input);
                })->select(FixedAssetType::pluck('name', 'id'))->width(2);
                $filter->date('inventory_date')->date()->width(2);
            });

            $titles = [
                'uid' => '创建人',
                'asset_number' => '物资编号',
                'asset_name' => '物资名称',
                'asset_spec' => '物资规格',
                'asset_type' => '物资分类',
                'price' => '单价',
                'inventory_date' => '入库日期',
                'amount' => '总数量',
                'amount_idle' => '闲置数量',
                'amount_using' => '在用数量',
                'explain' => '说明'
            ];
            $grid->export()->disableExportSelectedRow()->titles($titles)->rows(function ($rows) {
                foreach ($rows as &$row) {
                    $row['uid'] = $row['adminUser']['name'];
                    $row['asset_type'] = $row['fixedAssetType']['name'];
                }

                return $rows;
            })->filename('物资列表-' . date('YmdHis'));
        });
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     *
     * @return Show
     */
    protected function detail($id)
    {
        return Show::make($id, FixedAsset::with(['adminUser:id,name', 'fixedAssetType:id,name']), function (Show $show) {
            $show->field('id');
            $show->field('admin_user.name');
            $show->field('asset_number');
            $show->field('asset_name');
            $show->field('asset_spec');
            $show->field('price');
            $show->field('fixed_asset_type.name');
            $show->field('inventory_date');
            $show->field('amount');
            $show->field('amount_idle');
            $show->field('amount_using');
            $show->field('explain');
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Form::make(new FixedAsset(), function (Form $form) {
            $assetTye = FixedAssetType::pluck('name', 'id')->toArray();

            $form->display('id');
            $form->hidden('uid');
            $form->select('asset_type')->options($assetTye)->required();
            $form->text('asset_number');
            $form->text('asset_name')->required();
            $form->text('asset_spec')->required();
            $form->text('price')->default(0)->rules('required|numeric', [
                'required' => '价格必须填写',
                'numeric' => '价格必须全部为数字',
            ])->required();
            $form->hidden('inventory_date');
            $form->number('amount')->default(1)->required();
            $form->hidden('amount_idle');
            $form->text('explain');

            $form->display('created_at');
            $form->display('updated_at');

            $form->saving(function (Form $form) {
                $form->uid = Admin::user()->id;
                if ($form->isCreating()) {
                    $form->inventory_date = date('Y-m-d');
                    $form->amount_idle = $form->amount;
                }
                if ($form->isEditing()) {
                    $form->amount_idle += $form->amount - $form->model()->amount;
                }
                if (is_null($form->explain)) {
                    $form->explain = '';
                }
            });
        });
    }
}
