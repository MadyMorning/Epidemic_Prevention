<?php

namespace App\Admin\Controllers;

use App\Models\AdminUser;
use App\Models\Classes;
use Dcat\Admin\Form;
use Dcat\Admin\Grid;
use Dcat\Admin\Show;
use Dcat\Admin\Http\Controllers\AdminController;

class ClassesController extends AdminController
{
    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Grid::make(Classes::with(['teacher']), function (Grid $grid) {
            $grid->enableDialogCreate();
            $grid->showQuickEditButton();
            $grid->disableEditButton();
            $grid->disableViewButton();
            $grid->quickSearch(['faculties', 'class_name', 'teacher.name']);

            $grid->column('id')->sortable();
            $grid->column('faculties');
            $grid->column('class_name');
            $grid->column('teacher.name');
            $grid->column('created_at');
            $grid->column('updated_at')->sortable();
        });
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     *
     * @return Show
     */
    protected function detail($id)
    {
        return Show::make($id, new Classes(), function (Show $show) {
            $show->field('id');
            $show->field('faculties');
            $show->field('class_name');
            $show->field('headteacher');
            $show->field('created_at');
            $show->field('updated_at');
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Form::make(new Classes(), function (Form $form) {
            $form->display('id');
            $form->text('faculties')->required();
            $form->text('class_name')->required();
            $form->select('headteacher', '班主任')
                ->options(AdminUser::where('identity', 2)->pluck('name', 'id'))
                ->required();

            $form->display('created_at');
            $form->display('updated_at');
        });
    }
}
