<?php

namespace App\Admin\Controllers;

use Dcat\Admin\Admin;
use Dcat\Admin\Form;
use Dcat\Admin\Grid;
use Dcat\Admin\Show;
use Dcat\Admin\Http\Controllers\AdminController;

use App\Models\FixedAssetType;

class FixedAssetTypeController extends AdminController
{
    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Grid::make(FixedAssetType::with('adminUser:id,name'), function (Grid $grid) {
            $grid->disableDeleteButton(); // 禁用删除按钮
            $grid->disableViewButton(); // 禁用详情按钮
            $grid->enableDialogCreate(); // 开启弹窗创建表单
            // 显示快捷编辑按钮
            $grid->showQuickEditButton();
            // 去除普通编辑按钮
            $grid->disableEditButton();
            // 设置弹窗宽高，默认值为 '700px', '670px'
            $grid->setDialogFormDimensions('50%', '50%');

            $grid->column('id')->sortable();
            $grid->column('adminUser.name');
            $grid->column('name');
            $grid->column('created_at');
            $grid->column('updated_at')->sortable();
        });
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     *
     * @return Show
     */
    protected function detail($id)
    {
        return Show::make($id, FixedAssetType::with('adminUser:id,name'), function (Show $show) {
            $show->field('id');
            $show->field('admin_user.name');
            $show->field('name');
            $show->field('created_at');
            $show->field('updated_at');
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Form::make(new FixedAssetType(), function (Form $form) {
            $form->display('id');
            $form->hidden('uid');
            $form->text('name');
            $form->display('created_at');
            $form->display('updated_at');

            $form->saving(function (Form $form) {
                if ($form->isCreating()) {
                    $form->uid = Admin::user()->id;
                }
            });
        });
    }
}
