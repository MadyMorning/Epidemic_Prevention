<?php

namespace App\Admin\Controllers;

use App\Enums\FixedAssetApplyState;
use App\Models\FixedAsset;
use App\Models\FixedAssetApply;
use Dcat\Admin\Admin;
use Dcat\Admin\Form;
use Dcat\Admin\Grid;
use Dcat\Admin\Show;
use Dcat\Admin\Http\Controllers\AdminController;
use Illuminate\Support\Facades\DB;

class FixedAssetApplyController extends AdminController
{
    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Grid::make(FixedAssetApply::with(['adminUser:id,name', 'fixedAsset' => function ($query) {
            $query->select('id', DB::raw("CONCAT_WS('-',asset_name,asset_spec) AS asset"));
        }]), function (Grid $grid) {
            $grid->enableDialogCreate(); // 开启弹窗创建表单
            // 显示快捷编辑按钮
            $grid->showQuickEditButton();
            // 去除普通编辑按钮
            $grid->disableEditButton();
            // 设置弹窗宽高，默认值为 '700px', '670px'
            $grid->setDialogFormDimensions('50%', '50%');

            $grid->column('id')->sortable();
            $grid->column('adminUser.name');
            $grid->column('fixedAsset.asset');
            $grid->column('number');
            $grid->column('state')->using(FixedAssetApplyState::asSelectArray());
            $grid->column('opt_uid');
            $grid->column('opt_time');
            $grid->column('apply_time');

            $grid->filter(function (Grid\Filter $filter) {
                $purchase_list = FixedAsset::select('id', DB::raw("CONCAT_WS('-',asset_name,asset_spec) AS asset"))
                    ->orderByDesc('id')
                    ->get()
                    ->pluck('asset', 'id')
                    ->toArray();

                $filter->panel();

                $filter->like('adminUser.name')->width(2);
                $filter->equal('asset_id')->select($purchase_list)->width(3);
            });
        });
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     *
     * @return Show
     */
    protected function detail($id)
    {
        return Show::make($id, FixedAssetApply::with(['adminUser:id,name', 'fixedAsset' => function ($query) {
            $query->select('id', DB::raw("CONCAT_WS('-',asset_name,asset_spec) AS asset"));
        }]), function (Show $show) {
            $show->field('id');
            $show->field('admin_user.name');
            $show->field('fixed_asset.asset');
            $show->field('number');
            $show->field('state')->using(FixedAssetApplyState::asSelectArray());
            $show->field('opt_uid');
            $show->field('opt_time');
            $show->field('apply_time');
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $purchase_list = FixedAsset::where('amount_idle', '>', 0)
            ->select('id', DB::raw("CONCAT_WS('-',asset_name,asset_spec) AS asset"))
            ->orderByDesc('id')
            ->get()
            ->pluck('asset', 'id')
            ->toArray();

        return Form::make(new FixedAssetApply(), function (Form $form) use ($purchase_list) {
            $form->display('id');
            $form->hidden('uid');
            $form->select('asset_id')->options($purchase_list);
            $form->number('number');
            // $form->text('state');
            // $form->text('opt_uid');
            // $form->text('opt_time');
            $form->hidden('apply_time');

            $form->display('created_at');
            $form->display('updated_at');

            $form->saving(function (Form $form) {
                if ($form->isCreating()) {
                    $form->uid = Admin::user()->id;
                    $form->apply_time = now()->toDateTimeString();
                }

                $amount_idle = FixedAsset::where('id', $form->asset_id)->value('amount_idle');
                if ($amount_idle < $form->number) {
                    // 中断后续逻辑
                    return $form->response()->error('物资闲置数量不足，剩余数量：' . $amount_idle);
                }
            });
        });
    }
}
