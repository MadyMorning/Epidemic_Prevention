<?php

namespace App\Admin\Controllers;

use App\Enums\HealthySymptom;
use App\Models\HealthyClock;
use Dcat\Admin\Admin;
use Dcat\Admin\Form;
use Dcat\Admin\Grid;
use Dcat\Admin\Show;
use Dcat\Admin\Http\Controllers\AdminController;
use Dcat\Admin\Widgets\Card;

class HealthyClockController extends AdminController
{
    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Grid::make(HealthyClock::with(['adminUser']), function (Grid $grid) {
            $grid->disableViewButton();
            $grid->disableDeleteButton();
            // 只能创建一次数据
            $exists = HealthyClock::where(['uid' => Admin::user()->id, 'date' => date('Y-m-d')])->exists();
            if ($exists || Admin::user()->isAdministrator()) {
                $grid->disableCreateButton();
            }

            $grid->column('id')->sortable();
            $grid->column('adminUser.name');
            $grid->column('date');
            $grid->column('temperature');
            $grid->column('symptom')->display(function ($value) {
                return HealthySymptom::getDescription($value);
            });
            $grid->column('other')->limit(10, '...');
            $grid->column('created_at');
            $grid->column('updated_at')->sortable();

            $grid->export();
            $grid->filter(function (Grid\Filter $filter) {
                $filter->panel();

                $filter->like('name')->width(2);
                $filter->date('date')->width(2);
            });
        });
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     *
     * @return Show
     */
    protected function detail($id)
    {
        return Show::make($id, new HealthyClock(), function (Show $show) {
            $show->field('id');
            $show->field('date');
            $show->field('temperature');
            $show->field('symptom');
            $show->field('other');
            $show->field('created_at');
            $show->field('updated_at');
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Form::make(new HealthyClock(), function (Form $form) {
            $admin_user = Admin::user();

            $form->display('id');
            $form->display('uid')->default($admin_user->name);
            $form->hidden('date')->default(now());
            $form->decimal('temperature')->required();
            $form->radio('symptom')->options(HealthySymptom::asSelectArray())->default(HealthySymptom::NORMAL)->required();
            $form->textarea('other')->rules('max:100', [
                'max'   => '不能超过100个字',
            ])->placeholder('其他状况说明（选填）');

            $form->display('created_at');
            $form->display('updated_at');

            // 提交前触发
            $form->submitted(function (Form $form) use ($admin_user) {
                $form->uid = $admin_user->id;
            });
        });
    }
}
