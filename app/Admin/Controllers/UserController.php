<?php

namespace App\Admin\Controllers;

use App\Enums\DormitoryFloor;
use App\Models\AdminUser;
use App\Models\Classes;
use Dcat\Admin\Form;
use Dcat\Admin\Grid;
use Dcat\Admin\Http\Auth\Permission;
use Dcat\Admin\Http\Controllers\AdminController;
use Dcat\Admin\Show;
use Dcat\Admin\Support\Helper;
use Dcat\Admin\Widgets\Tree;

class UserController extends AdminController
{
    public function title()
    {
        return trans('admin.administrator');
    }

    protected function grid()
    {
        return Grid::make(AdminUser::with(['roles']), function (Grid $grid) {
            $grid->showQuickEditButton();
            $grid->enableDialogCreate();
            $grid->disableEditButton();
            $grid->quickSearch(['id', 'name', 'username']);

            $grid->actions(function (Grid\Displayers\Actions $actions) {
                if ($actions->getKey() == AdminUser::DEFAULT_ID) {
                    $actions->disableDelete();
                }
            });

            $grid->column('id', 'ID')->sortable();

            $grid->column('username');
            $grid->column('name');
            $grid->column('identity', trans('admin.identity'))->display(function ($value) {
                return [1 => '工作人员', 2 => '教师', 3 => '学生'][$value];
            });
            $grid->column('sex', trans('admin.sex'))->display(function ($value) {
                return [0 => '未知', 1 => '男', 2 => '女'][$value];
            });
            $grid->column('mobile', trans('admin.mobile'));
            $grid->column('state', trans('admin.state'))->display(function ($value) {
                return [1 => '正常', 2 => '禁用'][$value];
            });

            if (config('admin.permission.enable')) {
                $grid->column('roles')->pluck('name')->label('primary', 3);
            }

            $grid->column('created_at');

            $grid->filter(function ($filter) {
                $filter->panel();

                $filter->equal('identity', trans('admin.identity'))
                    ->select([1 => '工作人员', 2 => '教师', 3 => '学生'])
                    ->width(2);
                $filter->equal('state', trans('admin.state'))
                    ->select([1 => '正常', 2 => '禁用'])
                    ->width(2);
            });
        });
    }

    protected function detail($id)
    {
        return Show::make($id, AdminUser::with(['roles']), function (Show $show) {
            $show->field('id');
            $show->field('username');
            $show->field('name');

            $show->field('avatar', __('admin.avatar'))->image();

            if (config('admin.permission.enable')) {
                $show->field('roles')->as(function ($roles) {
                    if (!$roles) {
                        return;
                    }

                    return collect($roles)->pluck('name');
                })->label();

                $show->field('permissions')->unescape()->as(function () {
                    $roles = $this->roles->toArray();

                    $permissionModel = config('admin.database.permissions_model');
                    $roleModel = config('admin.database.roles_model');
                    $permissionModel = new $permissionModel();
                    $nodes = $permissionModel->allNodes();

                    $tree = Tree::make($nodes);

                    $isAdministrator = false;
                    foreach (array_column($roles, 'slug') as $slug) {
                        if ($roleModel::isAdministrator($slug)) {
                            $tree->checkAll();
                            $isAdministrator = true;
                        }
                    }

                    if (!$isAdministrator) {
                        $keyName = $permissionModel->getKeyName();
                        $tree->check(
                            $roleModel::getPermissionId(array_column($roles, $keyName))->flatten()
                        );
                    }

                    return $tree->render();
                });
            }

            $show->field('created_at');
            $show->field('updated_at');
        });
    }

    public function form()
    {
        return Form::make(AdminUser::with(['roles']), function (Form $form) {
            $userTable = config('admin.database.users_table');
            $connection = config('admin.database.connection');

            $id = $form->getKey();
            if ($id == AdminUser::DEFAULT_ID) {
                $form->disableDeleteButton();
            }

            $form->display('id', 'ID');
            $form->radio('identity', trans('admin.identity'))
                ->options([1 => '工作人员', 2 => '教师', 3 => '学生'])
                ->required();

            $form->text('username', trans('admin.username'))
                ->required()
                ->creationRules(['required', "unique:{$connection}.{$userTable}"])
                ->updateRules(['required', "unique:{$connection}.{$userTable},username,$id"]);
            $form->text('name', trans('admin.name'))->required();
            $form->image('avatar', trans('admin.avatar'))->autoUpload();
            $form->radio('sex', trans('admin.sex'))->options([0 => '未知', 1 => '男', 2 => '女'])->required()->default(0);
            $form->mobile('mobile', trans('admin.mobile'))->required();
            $form->select('class', trans('admin.class'))->options(Classes::pluck('class_name', 'id'));
            $form->radio('state', trans('admin.state'))->options([1 => '正常', 2 => '禁用'])->required()->default(1);
            $form->select('dormitory_floor', trans('admin.dormitory_floor'))->options(DormitoryFloor::asSelectArray());
            $form->text('doorplate', trans('admin.doorplate'));

            if ($id) {
                $form->password('password', trans('admin.password'))
                    ->minLength(5)
                    ->maxLength(20)
                    ->customFormat(function () {
                        return '';
                    });
            } else {
                $form->password('password', trans('admin.password'))
                    ->required()
                    ->minLength(5)
                    ->maxLength(20);
            }

            $form->password('password_confirmation', trans('admin.password_confirmation'))->same('password');

            $form->ignore(['password_confirmation']);

            if (config('admin.permission.enable')) {
                $form->multipleSelect('roles', trans('admin.roles'))
                    ->options(function () {
                        $roleModel = config('admin.database.roles_model');

                        return $roleModel::all()->pluck('name', 'id');
                    })
                    ->customFormat(function ($v) {
                        return array_column($v, 'id');
                    })
                    ->required();
            }

            $form->display('created_at', trans('admin.created_at'));
            $form->display('updated_at', trans('admin.updated_at'));
        })->saving(function (Form $form) {
            if ($form->password && $form->model()->get('password') != $form->password) {
                $form->password = bcrypt($form->password);
            }

            if (!$form->password) {
                $form->deleteInput('password');
            }
        });
    }

    public function destroy($id)
    {
        if (in_array(AdminUser::DEFAULT_ID, Helper::array($id))) {
            Permission::error();
        }

        return parent::destroy($id);
    }
}
