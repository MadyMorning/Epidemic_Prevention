<?php

namespace App\Admin\Controllers;

use App\Enums\VaccineNumber;
use App\Models\AdminUser;
use App\Models\Vaccine;
use Dcat\Admin\Admin;
use Dcat\Admin\Form;
use Dcat\Admin\Grid;
use Dcat\Admin\Show;
use Dcat\Admin\Http\Controllers\AdminController;

class VaccineController extends AdminController
{
    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Grid::make(Vaccine::with(['adminUser']), function (Grid $grid) {
            // 只能创建一次数据
            $exists = Vaccine::where('uid', Admin::user()->id)->exists();
            if ($exists == true || Admin::user()->isAdministrator()) {
                $grid->disableCreateButton();
            }
            $grid->column('id')->sortable();
            $grid->column('adminUser.name');
            $grid->column('number')->display(function ($value) {
                return VaccineNumber::getDescription($value);
            });
            $grid->column('first_at')->display(function ($value) {
                return ($value) ?? '无';
            });
            $grid->column('second_at')->display(function ($value) {
                return ($value) ?? '无';
            });
            $grid->column('third_at')->display(function ($value) {
                return ($value) ?? '无';
            });
            $grid->column('reason');
            $grid->column('created_at');

            $grid->filter(function (Grid\Filter $filter) {
                $filter->in('uid')->multipleSelect(AdminUser::pluck('name', 'id'));
                $filter->in('number')->multipleSelect(VaccineNumber::asSelectArray());
                $filter->between('first_at')->date();
                $filter->between('third_at')->date();
            });

            $grid->export();
            $grid->disableViewButton();
            $grid->disableDeleteButton();
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Form::make(new Vaccine(), function (Form $form) {
            $admin_user = Admin::user();
            $form->display('id');
            $form->display('uid')->default($admin_user->name);
            $options = VaccineNumber::asSelectArray();
            // 不能往回选
            if ($form->isEditing()) {
                for ($i = 0; $i <= 3; $i++) {
                    $number = $form->model()->number;
                    if ($number > $i) {
                        unset($options[$i]);
                    }
                }
            }
            $when = [
                'reason' => [VaccineNumber::ZERO, VaccineNumber::FIRST, VaccineNumber::SECOND],
                'first' => [VaccineNumber::FIRST, VaccineNumber::SECOND, VaccineNumber::THIRD],
                'second' => [VaccineNumber::SECOND, VaccineNumber::THIRD]
            ];
            $form->radio('number')
                ->when($when['first'], function (Form $form) use ($when) {
                    $form->date('first_at')->rules([
                        'required_if:number,' . implode(',', $when['first']),
                        'before:tomorrow'
                    ], [
                        'required_if' => '必须填写第一针接种日期',
                        'before' => '不能大于当前日期'
                    ]);
                })->when($when['second'], function (Form $form) use ($when) {
                    $form->date('second_at')->rules([
                        'required_if:number,' . implode(',',  $when['second']),
                        'after_or_equal:first_at',
                        'before:tomorrow'
                    ], [
                        'required_if' => '必须填写第二针接种日期',
                        'after_or_equal' => '第二针接种日期必须大于第一针接种日期',
                        'before' => '不能大于当前日期'
                    ]);
                })
                ->when($when['reason'], function (Form $form) use ($when) {
                    $form->textarea('reason')->rules([
                        'required_if:number,' . implode(',',  $when['reason']),
                        'max:200'
                    ], [
                        'required_if' => '必须填写未接种原因',
                        'max' => '请不要超过200个字'
                    ]);
                })
                ->when(VaccineNumber::THIRD, function (Form $form) {
                    $form->date('third_at')->rules([
                        'required_if:number,' . VaccineNumber::THIRD,
                        'after_or_equal:second_at',
                        'before:tomorrow'
                    ], [
                        'required_if' => '必须填写第三针接种日期',
                        'after_or_equal' => '第三针接种日期必须大于第二针接种日期',
                        'before' => '不能大于当前日期'
                    ]);
                })->options($options)->default(3)->required();
            // 提交前触发
            $form->submitted(function (Form $form) {
                $form->uid = Admin::user()->id;
                // 根据针数 清空对应字段
                switch ($form->number) {
                    case VaccineNumber::ZERO:
                        $form->first_at = '';
                        $form->second_at = '';
                        $form->third_at = '';
                        break;
                    case VaccineNumber::FIRST:
                        $form->second_at = '';
                        $form->third_at = '';
                        break;
                    case VaccineNumber::SECOND:
                        $form->third_at = '';
                        break;
                    case VaccineNumber::THIRD:
                        $form->reason = '';
                        break;
                }
            });
        });
    }
}
