<?php

namespace App\Admin\Controllers;

use App\Enums\QuarantineAnomalies;
use App\Models\AdminUser;
use App\Models\Quarantine;
use Dcat\Admin\Form;
use Dcat\Admin\Grid;
use Dcat\Admin\Show;
use Dcat\Admin\Http\Controllers\AdminController;
use Illuminate\Support\Carbon;

class QuarantineController extends AdminController
{
    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Grid::make(Quarantine::with('adminUser.classes'), function (Grid $grid) {
            $grid->disableViewButton();

            $grid->column('id')->sortable();
            $grid->column('adminUser.name');
            $grid->column('adminUser.mobile');
            $grid->column('adminUser.classes.class_name');
            $grid->column('adminUser.dormitory_floor');
            $grid->column('adminUser.doorplate');
            $grid->column('quarantine_reson');
            $grid->column('anomalies')->using(QuarantineAnomalies::asSelectArray());
            $grid->column('start_quarantine');
            $grid->column('end_quarantine');
            $grid->column('quarantine_days');
            $grid->column('remainder_days')->display(function () {
                $remainder_days = Carbon::parse(now()->toDateString())->diffInDays($this->end_quarantine, false);
                if ($remainder_days < 0) {
                    $remainder_days = '已结束';
                }

                return $remainder_days;
            });
            $grid->column('remark');

            $grid->filter(function (Grid\Filter $filter) {
                $filter->panel();

                $filter->like('adminUser.name')->width(2);
                $filter->like('adminUser.classes.class_name')->width(2);
                $filter->equal('anomalies')->select(QuarantineAnomalies::asSelectArray())->width(2);
            });

            $grid->export();
        });
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     *
     * @return Show
     */
    protected function detail($id)
    {
        return Show::make($id, new Quarantine(), function (Show $show) {
            $show->field('id');
            $show->field('uid');
            $show->field('quarantine_reson');
            $show->field('anomalies');
            $show->field('start_quarantine');
            $show->field('end_quarantine');
            $show->field('quarantine_days');
            $show->field('remark');
            $show->field('created_at');
            $show->field('updated_at');
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Form::make(new Quarantine(), function (Form $form) {
            $form->display('id');
            $form->select('uid')->options(AdminUser::pluck('name', 'id'))->required();
            $form->text('quarantine_reson')->required();
            $form->select('anomalies')->options(QuarantineAnomalies::asSelectArray())->display(QuarantineAnomalies::ONE)->required();
            $form->dateRange('start_quarantine', 'end_quarantine', '隔离时间')->required();
            $form->hidden('quarantine_days');
            $form->text('remark');

            $form->display('created_at');
            $form->display('updated_at');

            $form->saving(function (Form $form) {
                $form->input('quarantine_days', Carbon::parse($form->end_quarantine)->diffInDays($form->start_quarantine));
            });
        });
    }
}
