<?php

use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Route;
use Dcat\Admin\Admin;

Admin::routes();

Route::group([
    'prefix'     => config('admin.route.prefix'),
    'namespace'  => config('admin.route.namespace'),
    'middleware' => config('admin.route.middleware'),
], function (Router $router) {
    $router->resource('auth/users', 'UserController');
    $router->resource('auth/menu', 'MenuController');
    $router->resource('auth/roles', 'RoleController');

    $router->get('/', 'HomeController@index');

    $router->resource('class', 'ClassesController');
    $router->resource('vaccine', 'VaccineController');
    $router->resource('healthy-clock', 'HealthyClockController');
    $router->resource('notice', 'NoticeController');
    $router->resource('quarantine', 'QuarantineController');
    $router->resource('fixed-asset/asset', 'FixedAssetController');
    $router->resource('fixed-asset/type', 'FixedAssetTypeController');
    $router->resource('fixed-asset/apply', 'FixedAssetApplyController');

});
