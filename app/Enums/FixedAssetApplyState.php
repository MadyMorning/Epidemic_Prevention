<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class FixedAssetApplyState extends Enum
{
    const DOING = 1;
    const DONE = 2;
    const REFUSE = 3;

    public static function getDescription($value): string
    {
        switch ($value) {
            case self::DOING:
                return '未审核';
                break;
            case self::DONE:
                return '已通过';
                break;
            case self::REFUSE:
                return '已拒绝';
                break;
        }
        return parent::getDescription($value);
    }
}
