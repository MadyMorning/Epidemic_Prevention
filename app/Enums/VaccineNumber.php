<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class VaccineNumber extends Enum
{
    const ZERO = 0;
    const FIRST = 1;
    const SECOND = 2;
    const THIRD = 3;

    public static function getDescription($value): string
    {
        switch ($value) {
            case self::ZERO:
                return '未接种';
                break;
            case self::FIRST:
                return '第一针';
                break;
            case self::SECOND:
                return '第二针';
                break;
            case self::THIRD:
                return '第三针';
                break;
        }
        return parent::getDescription($value);
    }
}
