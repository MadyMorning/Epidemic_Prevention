<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class HealthySymptom extends Enum
{
    const NORMAL = 1;
    const FEVER = 2;
    const COUGH = 3;
    const WEAK = 4;

    public static function getDescription($value): string
    {
        switch ($value) {
            case self::NORMAL:
                return '正常';
                break;
            case self::FEVER:
                return '发烧';
                break;
            case self::COUGH:
                return '咳嗽';
                break;
            case self::WEAK:
                return '乏力';
                break;
        }
        return parent::getDescription($value);
    }
}
