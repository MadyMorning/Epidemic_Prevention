<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class QuarantineAnomalies extends Enum
{
    const ONE = 1;
    const TWO = 2;
    const THREE = 3;

    public static function getDescription($value): string
    {
        switch ($value) {
            case self::ONE:
                return '健康';
                break;
            case self::TWO:
                return '疑似';
                break;
            case self::THREE:
                return '确诊';
                break;
        }
        return parent::getDescription($value);
    }
}
