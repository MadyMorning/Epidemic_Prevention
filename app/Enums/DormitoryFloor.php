<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class DormitoryFloor extends Enum
{
    const ONE = 1;
    const TWO = 2;
    const THREE = 3;

    public static function getDescription($value): string
    {
        switch ($value) {
            case self::ONE:
                return '1号楼';
                break;
            case self::TWO:
                return '2号楼';
                break;
            case self::THREE:
                return '3号楼';
                break;
        }
        return parent::getDescription($value);
    }
}
