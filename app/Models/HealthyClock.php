<?php

namespace App\Models;

use Dcat\Admin\Traits\HasDateTimeFormatter;

use Illuminate\Database\Eloquent\Model;

class HealthyClock extends Model
{
    use HasDateTimeFormatter;
    protected $table = 'healthy_clock';

    public function adminUser()
    {
        return $this->hasOne(AdminUser::class, 'id', 'uid');
    }
}
