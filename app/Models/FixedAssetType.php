<?php

namespace App\Models;

use Dcat\Admin\Traits\HasDateTimeFormatter;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FixedAssetType extends Model
{
    use HasFactory;
    use HasDateTimeFormatter;

    protected $table = 'fixed_asset_type';

    /**
     * 关联用户表
     *
     */
    public function adminUser()
    {
        return $this->hasOne(AdminUser::class, 'id', 'uid');
    }

    /**
     * 关联资产列表表
     *
     */
    public function fixedAsset()
    {
        return $this->hasMany(FixedAsset::class, 'asset_type', 'id');
    }
}
