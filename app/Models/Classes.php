<?php

namespace App\Models;

use Dcat\Admin\Traits\HasDateTimeFormatter;

use Illuminate\Database\Eloquent\Model;

class Classes extends Model
{
    use HasDateTimeFormatter;

    protected $table = 'classes';

    public function teacher()
    {
        return $this->hasOne(AdminUser::class, 'id', 'headteacher');
    }
}
