<?php

namespace App\Models;

use Dcat\Admin\Traits\HasDateTimeFormatter;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Vaccine extends Model
{
    use HasDateTimeFormatter;
    use SoftDeletes;

    public function adminUser()
    {
        return $this->hasOne(AdminUser::class, 'id', 'uid');
    }
}
