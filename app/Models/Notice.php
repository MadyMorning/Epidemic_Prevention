<?php

namespace App\Models;

use Dcat\Admin\Traits\HasDateTimeFormatter;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class Notice extends Model
{
    use HasDateTimeFormatter;

    protected $table = 'notice';

    public static function getList()
    {
        $res = self::with('admin_user:id,name')
            ->where(function ($query) {
                $query->whereDate('start_time', '<=', now()->toDateString())
                    ->orWhereNull('start_time');

            })
            ->where(function ($query){
                $query->whereDate('end_time', '>=', now()->toDateString())
                    ->orWhereNull('end_time');
            })
            ->orderBy('created_at', 'desc')
            ->select('id', 'img_url', 'title', 'start_time', 'admin_id')
            ->limit(5)
            ->get()
            ->toArray();
        return $res;
    }

    public function admin_user()
    {
        return $this->belongsTo(AdminUser::class, 'admin_id', 'id');
    }

    // 获取图片路径
    public static function getImage($value)
    {
        if (Str::contains($value, '//')) {
            return $value;
        }
        $disk = Storage::disk(config('admin.upload.disk'));
        return ($disk->exists($value)) ? $disk->url($value) : $disk->url('/default/icons/notice.png');
    }

    // 获取文件路径
    public static function getFile($value)
    {
        if (Str::contains($value, '//')) {
            return $value;
        }
        $disk = Storage::disk(config('admin.upload.disk'));
        return ($disk->exists($value)) ? $disk->url($value) : '';
    }
}
