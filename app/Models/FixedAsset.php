<?php

namespace App\Models;

use Dcat\Admin\Traits\HasDateTimeFormatter;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FixedAsset extends Model
{
    use HasFactory;
    use HasDateTimeFormatter;

    protected $table = 'fixed_asset';

    protected $guarded = [];

    public function adminUser()
    {
        return $this->hasOne(AdminUser::class, 'id', 'uid');
    }

    public function fixedAssetType()
    {
        return $this->hasOne(FixedAssetType::class, 'id', 'asset_type');
    }
}
