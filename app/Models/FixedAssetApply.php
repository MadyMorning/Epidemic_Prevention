<?php

namespace App\Models;

use Dcat\Admin\Traits\HasDateTimeFormatter;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class FixedAssetApply extends Model
{
	use HasDateTimeFormatter;
    use SoftDeletes;

    protected $table = 'fixed_asset_apply';

    public function adminUser()
    {
        return $this->hasOne(AdminUser::class, 'id', 'uid');
    }

    public function fixedAsset()
    {
        return $this->hasOne(FixedAsset::class, 'id', 'asset_id');
    }
}
