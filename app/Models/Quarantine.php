<?php

namespace App\Models;

use Dcat\Admin\Traits\HasDateTimeFormatter;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Quarantine extends Model
{
	use HasDateTimeFormatter;
    use SoftDeletes;

    protected $table = 'quarantine';

    public function adminUser()
    {
        return $this->hasOne(AdminUser::class, 'id', 'uid');
    }
}
