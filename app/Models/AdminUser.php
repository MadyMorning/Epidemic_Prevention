<?php

namespace App\Models;

use Dcat\Admin\Models\Administrator;

class AdminUser extends Administrator
{
    public function classes()
    {
        return $this->hasOne(Classes::class, 'id', 'class');
    }
}
