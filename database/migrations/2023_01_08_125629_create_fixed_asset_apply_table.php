<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFixedAssetApplyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fixed_asset_apply', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('uid')->index()->comment('申请人');
            $table->integer('asset_id')->comment('申请物资');
            $table->unsignedTinyInteger('number')->comment('申请数量');
            $table->unsignedTinyInteger('state')->default('1')->comment('状态 1:未审核 2:已通过 3:已拒绝');
            $table->integer('opt_uid')->default('0')->nullable()->comment('审核人');
            $table->dateTime('opt_time')->nullable()->comment('审核时间');
            $table->dateTime('apply_time');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fixed_asset_apply');
    }
}
