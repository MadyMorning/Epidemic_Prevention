<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuarantineTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quarantine', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('uid')->index()->comment('用户');
            $table->char('mobile')->comment('手机号');
            $table->string('quarantine_reson')->default('')->comment('隔离原因');
            $table->tinyInteger('anomalies')->comment('隔离情况 1:健康 2:疑似 3:确诊');
            $table->date('start_quarantine')->comment('开始隔离日期');
            $table->date('end_quarantine')->comment('结束隔离日期');
            $table->tinyInteger('quarantine_days')->comment('隔离天数');
            $table->string('remark')->default('')->comment('备注');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quarantine');
    }
}
