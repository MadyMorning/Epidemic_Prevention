<?php

use App\Enums\NoticeType;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateNoticesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notice', function (Blueprint $table) {
            $table->id();
            $table->string('title', 50)->comment('标题');
            $table->string('img_url', 100)->nullable()->default('')->comment('封面图片');
            $table->tinyInteger('notice_type')->comment('公告类型');
            $table->text('content')->comment('公告内容');
            $table->string('file_url', 100)->nullable()->default('')->comment('附件');
            $table->date('start_time')->nullable()->comment('展示开始日期');
            $table->date('end_time')->nullable()->comment('展示结束日期');
            $table->integer('admin_id')->comment('创建人');

            $table->timestamps();
            $table->softDeletes();
        });
        DB::statement("ALTER TABLE `notice` comment '公告'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notice');
    }
}
