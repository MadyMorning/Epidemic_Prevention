<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFixedAssetTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fixed_asset_type', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('uid')->index()->comment('创建人');
            $table->integer('parent_id')->default('0')->comment('上级');
            $table->string('name', 32)->comment('名称');
            $table->integer('order')->default('0')->comment('排序');

            $table->timestamps();
            $table->softDeletes();
        });

        DB::statement("ALTER TABLE `fixed_asset_type` comment '物资类型'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fixed_asset_type');
    }
}
