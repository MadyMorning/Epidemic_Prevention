<?php

use App\Enums\FixedAssetCategory;
use App\Enums\FixedAssetIsFixed;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFixedAssetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fixed_asset', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('uid')->index()->comment('创建人');
            $table->string('asset_number', 50)->nullable()->default('')->comment('物资编号');
            $table->string('asset_name', 32)->comment('物资名称');
            $table->string('asset_spec', 50)->comment('物资规格');
            $table->tinyInteger('asset_type')->comment('物资分类');
            $table->decimal('price', 10)->default(0.00)->comment('价格');
            $table->date('inventory_date')->comment('日期');
            $table->smallInteger('amount')->default(0)->comment('总数量');
            $table->smallInteger('amount_idle')->default(0)->comment('闲置数量');
            $table->smallInteger('amount_using')->default(0)->comment('在用数量');
            $table->string('explain', 100)->default('')->comment('说明');

            $table->timestamps();
            $table->softDeletes();
        });

        DB::statement("ALTER TABLE `fixed_asset` comment '物资列表'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fixed_asset');
    }
}
