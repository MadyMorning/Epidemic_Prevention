<?php

/**
 * A helper file for Dcat Admin, to provide autocomplete information to your IDE
 *
 * This file should not be included in your code, only analyzed by your IDE!
 *
 * @author jqh <841324345@qq.com>
 */
namespace Dcat\Admin {
    use Illuminate\Support\Collection;

    /**
     * @property Grid\Column|Collection id
     * @property Grid\Column|Collection name
     * @property Grid\Column|Collection type
     * @property Grid\Column|Collection version
     * @property Grid\Column|Collection detail
     * @property Grid\Column|Collection created_at
     * @property Grid\Column|Collection updated_at
     * @property Grid\Column|Collection is_enabled
     * @property Grid\Column|Collection parent_id
     * @property Grid\Column|Collection order
     * @property Grid\Column|Collection icon
     * @property Grid\Column|Collection uri
     * @property Grid\Column|Collection extension
     * @property Grid\Column|Collection permission_id
     * @property Grid\Column|Collection menu_id
     * @property Grid\Column|Collection slug
     * @property Grid\Column|Collection http_method
     * @property Grid\Column|Collection http_path
     * @property Grid\Column|Collection role_id
     * @property Grid\Column|Collection user_id
     * @property Grid\Column|Collection value
     * @property Grid\Column|Collection username
     * @property Grid\Column|Collection password
     * @property Grid\Column|Collection avatar
     * @property Grid\Column|Collection identity
     * @property Grid\Column|Collection sex
     * @property Grid\Column|Collection mobile
     * @property Grid\Column|Collection class
     * @property Grid\Column|Collection state
     * @property Grid\Column|Collection age
     * @property Grid\Column|Collection dormitory_floor
     * @property Grid\Column|Collection doorplate
     * @property Grid\Column|Collection remember_token
     * @property Grid\Column|Collection faculties
     * @property Grid\Column|Collection class_name
     * @property Grid\Column|Collection headteacher
     * @property Grid\Column|Collection uuid
     * @property Grid\Column|Collection connection
     * @property Grid\Column|Collection queue
     * @property Grid\Column|Collection payload
     * @property Grid\Column|Collection exception
     * @property Grid\Column|Collection failed_at
     * @property Grid\Column|Collection uid
     * @property Grid\Column|Collection asset_number
     * @property Grid\Column|Collection asset_name
     * @property Grid\Column|Collection asset_spec
     * @property Grid\Column|Collection asset_type
     * @property Grid\Column|Collection price
     * @property Grid\Column|Collection inventory_date
     * @property Grid\Column|Collection amount
     * @property Grid\Column|Collection amount_idle
     * @property Grid\Column|Collection amount_using
     * @property Grid\Column|Collection explain
     * @property Grid\Column|Collection deleted_at
     * @property Grid\Column|Collection asset_id
     * @property Grid\Column|Collection opt_uid
     * @property Grid\Column|Collection opt_time
     * @property Grid\Column|Collection apply_time
     * @property Grid\Column|Collection date
     * @property Grid\Column|Collection temperature
     * @property Grid\Column|Collection symptom
     * @property Grid\Column|Collection other
     * @property Grid\Column|Collection img_url
     * @property Grid\Column|Collection content
     * @property Grid\Column|Collection file_url
     * @property Grid\Column|Collection start_time
     * @property Grid\Column|Collection end_time
     * @property Grid\Column|Collection admin_id
     * @property Grid\Column|Collection email
     * @property Grid\Column|Collection token
     * @property Grid\Column|Collection tokenable_type
     * @property Grid\Column|Collection tokenable_id
     * @property Grid\Column|Collection abilities
     * @property Grid\Column|Collection last_used_at
     * @property Grid\Column|Collection quarantine_reson
     * @property Grid\Column|Collection anomalies
     * @property Grid\Column|Collection start_quarantine
     * @property Grid\Column|Collection end_quarantine
     * @property Grid\Column|Collection quarantine_days
     * @property Grid\Column|Collection remark
     * @property Grid\Column|Collection email_verified_at
     * @property Grid\Column|Collection first_at
     * @property Grid\Column|Collection second_at
     * @property Grid\Column|Collection third_at
     * @property Grid\Column|Collection reason
     *
     * @method Grid\Column|Collection id(string $label = null)
     * @method Grid\Column|Collection name(string $label = null)
     * @method Grid\Column|Collection type(string $label = null)
     * @method Grid\Column|Collection version(string $label = null)
     * @method Grid\Column|Collection detail(string $label = null)
     * @method Grid\Column|Collection created_at(string $label = null)
     * @method Grid\Column|Collection updated_at(string $label = null)
     * @method Grid\Column|Collection is_enabled(string $label = null)
     * @method Grid\Column|Collection parent_id(string $label = null)
     * @method Grid\Column|Collection order(string $label = null)
     * @method Grid\Column|Collection icon(string $label = null)
     * @method Grid\Column|Collection uri(string $label = null)
     * @method Grid\Column|Collection extension(string $label = null)
     * @method Grid\Column|Collection permission_id(string $label = null)
     * @method Grid\Column|Collection menu_id(string $label = null)
     * @method Grid\Column|Collection slug(string $label = null)
     * @method Grid\Column|Collection http_method(string $label = null)
     * @method Grid\Column|Collection http_path(string $label = null)
     * @method Grid\Column|Collection role_id(string $label = null)
     * @method Grid\Column|Collection user_id(string $label = null)
     * @method Grid\Column|Collection value(string $label = null)
     * @method Grid\Column|Collection username(string $label = null)
     * @method Grid\Column|Collection password(string $label = null)
     * @method Grid\Column|Collection avatar(string $label = null)
     * @method Grid\Column|Collection identity(string $label = null)
     * @method Grid\Column|Collection sex(string $label = null)
     * @method Grid\Column|Collection mobile(string $label = null)
     * @method Grid\Column|Collection class(string $label = null)
     * @method Grid\Column|Collection state(string $label = null)
     * @method Grid\Column|Collection age(string $label = null)
     * @method Grid\Column|Collection dormitory_floor(string $label = null)
     * @method Grid\Column|Collection doorplate(string $label = null)
     * @method Grid\Column|Collection remember_token(string $label = null)
     * @method Grid\Column|Collection faculties(string $label = null)
     * @method Grid\Column|Collection class_name(string $label = null)
     * @method Grid\Column|Collection headteacher(string $label = null)
     * @method Grid\Column|Collection uuid(string $label = null)
     * @method Grid\Column|Collection connection(string $label = null)
     * @method Grid\Column|Collection queue(string $label = null)
     * @method Grid\Column|Collection payload(string $label = null)
     * @method Grid\Column|Collection exception(string $label = null)
     * @method Grid\Column|Collection failed_at(string $label = null)
     * @method Grid\Column|Collection uid(string $label = null)
     * @method Grid\Column|Collection asset_number(string $label = null)
     * @method Grid\Column|Collection asset_name(string $label = null)
     * @method Grid\Column|Collection asset_spec(string $label = null)
     * @method Grid\Column|Collection asset_type(string $label = null)
     * @method Grid\Column|Collection price(string $label = null)
     * @method Grid\Column|Collection inventory_date(string $label = null)
     * @method Grid\Column|Collection amount(string $label = null)
     * @method Grid\Column|Collection amount_idle(string $label = null)
     * @method Grid\Column|Collection amount_using(string $label = null)
     * @method Grid\Column|Collection explain(string $label = null)
     * @method Grid\Column|Collection deleted_at(string $label = null)
     * @method Grid\Column|Collection asset_id(string $label = null)
     * @method Grid\Column|Collection opt_uid(string $label = null)
     * @method Grid\Column|Collection opt_time(string $label = null)
     * @method Grid\Column|Collection apply_time(string $label = null)
     * @method Grid\Column|Collection date(string $label = null)
     * @method Grid\Column|Collection temperature(string $label = null)
     * @method Grid\Column|Collection symptom(string $label = null)
     * @method Grid\Column|Collection other(string $label = null)
     * @method Grid\Column|Collection img_url(string $label = null)
     * @method Grid\Column|Collection content(string $label = null)
     * @method Grid\Column|Collection file_url(string $label = null)
     * @method Grid\Column|Collection start_time(string $label = null)
     * @method Grid\Column|Collection end_time(string $label = null)
     * @method Grid\Column|Collection admin_id(string $label = null)
     * @method Grid\Column|Collection email(string $label = null)
     * @method Grid\Column|Collection token(string $label = null)
     * @method Grid\Column|Collection tokenable_type(string $label = null)
     * @method Grid\Column|Collection tokenable_id(string $label = null)
     * @method Grid\Column|Collection abilities(string $label = null)
     * @method Grid\Column|Collection last_used_at(string $label = null)
     * @method Grid\Column|Collection quarantine_reson(string $label = null)
     * @method Grid\Column|Collection anomalies(string $label = null)
     * @method Grid\Column|Collection start_quarantine(string $label = null)
     * @method Grid\Column|Collection end_quarantine(string $label = null)
     * @method Grid\Column|Collection quarantine_days(string $label = null)
     * @method Grid\Column|Collection remark(string $label = null)
     * @method Grid\Column|Collection email_verified_at(string $label = null)
     * @method Grid\Column|Collection first_at(string $label = null)
     * @method Grid\Column|Collection second_at(string $label = null)
     * @method Grid\Column|Collection third_at(string $label = null)
     * @method Grid\Column|Collection reason(string $label = null)
     */
    class Grid {}

    class MiniGrid extends Grid {}

    /**
     * @property Show\Field|Collection id
     * @property Show\Field|Collection name
     * @property Show\Field|Collection type
     * @property Show\Field|Collection version
     * @property Show\Field|Collection detail
     * @property Show\Field|Collection created_at
     * @property Show\Field|Collection updated_at
     * @property Show\Field|Collection is_enabled
     * @property Show\Field|Collection parent_id
     * @property Show\Field|Collection order
     * @property Show\Field|Collection icon
     * @property Show\Field|Collection uri
     * @property Show\Field|Collection extension
     * @property Show\Field|Collection permission_id
     * @property Show\Field|Collection menu_id
     * @property Show\Field|Collection slug
     * @property Show\Field|Collection http_method
     * @property Show\Field|Collection http_path
     * @property Show\Field|Collection role_id
     * @property Show\Field|Collection user_id
     * @property Show\Field|Collection value
     * @property Show\Field|Collection username
     * @property Show\Field|Collection password
     * @property Show\Field|Collection avatar
     * @property Show\Field|Collection identity
     * @property Show\Field|Collection sex
     * @property Show\Field|Collection mobile
     * @property Show\Field|Collection class
     * @property Show\Field|Collection state
     * @property Show\Field|Collection age
     * @property Show\Field|Collection dormitory_floor
     * @property Show\Field|Collection doorplate
     * @property Show\Field|Collection remember_token
     * @property Show\Field|Collection faculties
     * @property Show\Field|Collection class_name
     * @property Show\Field|Collection headteacher
     * @property Show\Field|Collection uuid
     * @property Show\Field|Collection connection
     * @property Show\Field|Collection queue
     * @property Show\Field|Collection payload
     * @property Show\Field|Collection exception
     * @property Show\Field|Collection failed_at
     * @property Show\Field|Collection uid
     * @property Show\Field|Collection asset_number
     * @property Show\Field|Collection asset_name
     * @property Show\Field|Collection asset_spec
     * @property Show\Field|Collection asset_type
     * @property Show\Field|Collection price
     * @property Show\Field|Collection inventory_date
     * @property Show\Field|Collection amount
     * @property Show\Field|Collection amount_idle
     * @property Show\Field|Collection amount_using
     * @property Show\Field|Collection explain
     * @property Show\Field|Collection deleted_at
     * @property Show\Field|Collection asset_id
     * @property Show\Field|Collection opt_uid
     * @property Show\Field|Collection opt_time
     * @property Show\Field|Collection apply_time
     * @property Show\Field|Collection date
     * @property Show\Field|Collection temperature
     * @property Show\Field|Collection symptom
     * @property Show\Field|Collection other
     * @property Show\Field|Collection img_url
     * @property Show\Field|Collection content
     * @property Show\Field|Collection file_url
     * @property Show\Field|Collection start_time
     * @property Show\Field|Collection end_time
     * @property Show\Field|Collection admin_id
     * @property Show\Field|Collection email
     * @property Show\Field|Collection token
     * @property Show\Field|Collection tokenable_type
     * @property Show\Field|Collection tokenable_id
     * @property Show\Field|Collection abilities
     * @property Show\Field|Collection last_used_at
     * @property Show\Field|Collection quarantine_reson
     * @property Show\Field|Collection anomalies
     * @property Show\Field|Collection start_quarantine
     * @property Show\Field|Collection end_quarantine
     * @property Show\Field|Collection quarantine_days
     * @property Show\Field|Collection remark
     * @property Show\Field|Collection email_verified_at
     * @property Show\Field|Collection first_at
     * @property Show\Field|Collection second_at
     * @property Show\Field|Collection third_at
     * @property Show\Field|Collection reason
     *
     * @method Show\Field|Collection id(string $label = null)
     * @method Show\Field|Collection name(string $label = null)
     * @method Show\Field|Collection type(string $label = null)
     * @method Show\Field|Collection version(string $label = null)
     * @method Show\Field|Collection detail(string $label = null)
     * @method Show\Field|Collection created_at(string $label = null)
     * @method Show\Field|Collection updated_at(string $label = null)
     * @method Show\Field|Collection is_enabled(string $label = null)
     * @method Show\Field|Collection parent_id(string $label = null)
     * @method Show\Field|Collection order(string $label = null)
     * @method Show\Field|Collection icon(string $label = null)
     * @method Show\Field|Collection uri(string $label = null)
     * @method Show\Field|Collection extension(string $label = null)
     * @method Show\Field|Collection permission_id(string $label = null)
     * @method Show\Field|Collection menu_id(string $label = null)
     * @method Show\Field|Collection slug(string $label = null)
     * @method Show\Field|Collection http_method(string $label = null)
     * @method Show\Field|Collection http_path(string $label = null)
     * @method Show\Field|Collection role_id(string $label = null)
     * @method Show\Field|Collection user_id(string $label = null)
     * @method Show\Field|Collection value(string $label = null)
     * @method Show\Field|Collection username(string $label = null)
     * @method Show\Field|Collection password(string $label = null)
     * @method Show\Field|Collection avatar(string $label = null)
     * @method Show\Field|Collection identity(string $label = null)
     * @method Show\Field|Collection sex(string $label = null)
     * @method Show\Field|Collection mobile(string $label = null)
     * @method Show\Field|Collection class(string $label = null)
     * @method Show\Field|Collection state(string $label = null)
     * @method Show\Field|Collection age(string $label = null)
     * @method Show\Field|Collection dormitory_floor(string $label = null)
     * @method Show\Field|Collection doorplate(string $label = null)
     * @method Show\Field|Collection remember_token(string $label = null)
     * @method Show\Field|Collection faculties(string $label = null)
     * @method Show\Field|Collection class_name(string $label = null)
     * @method Show\Field|Collection headteacher(string $label = null)
     * @method Show\Field|Collection uuid(string $label = null)
     * @method Show\Field|Collection connection(string $label = null)
     * @method Show\Field|Collection queue(string $label = null)
     * @method Show\Field|Collection payload(string $label = null)
     * @method Show\Field|Collection exception(string $label = null)
     * @method Show\Field|Collection failed_at(string $label = null)
     * @method Show\Field|Collection uid(string $label = null)
     * @method Show\Field|Collection asset_number(string $label = null)
     * @method Show\Field|Collection asset_name(string $label = null)
     * @method Show\Field|Collection asset_spec(string $label = null)
     * @method Show\Field|Collection asset_type(string $label = null)
     * @method Show\Field|Collection price(string $label = null)
     * @method Show\Field|Collection inventory_date(string $label = null)
     * @method Show\Field|Collection amount(string $label = null)
     * @method Show\Field|Collection amount_idle(string $label = null)
     * @method Show\Field|Collection amount_using(string $label = null)
     * @method Show\Field|Collection explain(string $label = null)
     * @method Show\Field|Collection deleted_at(string $label = null)
     * @method Show\Field|Collection asset_id(string $label = null)
     * @method Show\Field|Collection opt_uid(string $label = null)
     * @method Show\Field|Collection opt_time(string $label = null)
     * @method Show\Field|Collection apply_time(string $label = null)
     * @method Show\Field|Collection date(string $label = null)
     * @method Show\Field|Collection temperature(string $label = null)
     * @method Show\Field|Collection symptom(string $label = null)
     * @method Show\Field|Collection other(string $label = null)
     * @method Show\Field|Collection img_url(string $label = null)
     * @method Show\Field|Collection content(string $label = null)
     * @method Show\Field|Collection file_url(string $label = null)
     * @method Show\Field|Collection start_time(string $label = null)
     * @method Show\Field|Collection end_time(string $label = null)
     * @method Show\Field|Collection admin_id(string $label = null)
     * @method Show\Field|Collection email(string $label = null)
     * @method Show\Field|Collection token(string $label = null)
     * @method Show\Field|Collection tokenable_type(string $label = null)
     * @method Show\Field|Collection tokenable_id(string $label = null)
     * @method Show\Field|Collection abilities(string $label = null)
     * @method Show\Field|Collection last_used_at(string $label = null)
     * @method Show\Field|Collection quarantine_reson(string $label = null)
     * @method Show\Field|Collection anomalies(string $label = null)
     * @method Show\Field|Collection start_quarantine(string $label = null)
     * @method Show\Field|Collection end_quarantine(string $label = null)
     * @method Show\Field|Collection quarantine_days(string $label = null)
     * @method Show\Field|Collection remark(string $label = null)
     * @method Show\Field|Collection email_verified_at(string $label = null)
     * @method Show\Field|Collection first_at(string $label = null)
     * @method Show\Field|Collection second_at(string $label = null)
     * @method Show\Field|Collection third_at(string $label = null)
     * @method Show\Field|Collection reason(string $label = null)
     */
    class Show {}

    /**
     
     */
    class Form {}

}

namespace Dcat\Admin\Grid {
    /**
     
     */
    class Column {}

    /**
     
     */
    class Filter {}
}

namespace Dcat\Admin\Show {
    /**
     
     */
    class Field {}
}
