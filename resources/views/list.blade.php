<ul class="list-group list-group-flush">
    @foreach ($list as $key => $item)
        {{-- 普通字段 --}}
        <li class="list-group-item d-flex"><span>{{ $key }}：</span>
            <div class="ordinary-field">{!! $item !!}</div>
        </li>
    @endforeach
</ul>

<style>
    .ordinary-field {
        overflow: hidden;
        width: 90%;
    }
    .dynamic-ordinary-field {
        word-break: break-all;
    }
</style>
