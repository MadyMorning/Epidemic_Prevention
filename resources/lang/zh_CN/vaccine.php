<?php
return [
    'labels' => [
        'Vaccine' => '疫苗接种信息',
        'vaccine' => '疫苗接种信息',
    ],
    'fields' => [
        'uid' => '姓名',
        'number' => '接种针数',
        'first_at' => '第一针接种日期',
        'second_at' => '第二针接种日期',
        'third_at' => '第三针接种日期',
        'reason' => '未接种原因',
        'adminUser' => [
            'name' => '姓名'
        ],
        'admin_user' => [
            'name' => '姓名'
        ]
    ],
    'options' => [
    ],
];
