<?php
return [
    'labels' => [
        'FixedAsset' => '物资列表',
        'fixed-asset' => '物资',
        'asset' => '列表',
    ],
    'fields' => [
        'uid' => '创建人',
        'asset_number' => '物资编号',
        'asset_name' => '物资名称',
        'asset_spec' => '物资规格',
        'asset_type' => '物资类型',
        'price' => '单价',
        'amount' => '总数量',
        'amount_idle' => '闲置数量',
        'amount_using' => '在用数量',
        'explain' => '说明',
        'inventory_date' => '入库日期',
        'adminUser' => [
            'name' => '创建人'
        ],
        'fixedAssetType' => [
            'name' => '物资类型'
        ],
        'admin_user' => [
            'name' => '创建人'
        ],
        'fixed_asset_type' => [
            'name' => '物资类型'
        ]
    ],
    'options' => [],
];
