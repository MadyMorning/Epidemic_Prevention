<?php

use App\Enums\HealthySymptom;

return [
    'labels' => [
        'HealthyClock' => '每日健康打卡',
        'healthy-clock' => '每日健康打卡',
    ],
    'fields' => [
        'date' => '日期',
        'temperature' => '体温',
        'symptom' => '症状',
        'other' => '其他状况说明',
        'adminUser' => [
            'name' => '姓名'
        ],
        'admin_user' => [
            'name' => '姓名'
        ]
    ],
    'options' => [
        'symptom' => HealthySymptom::asSelectArray()
    ],
];
