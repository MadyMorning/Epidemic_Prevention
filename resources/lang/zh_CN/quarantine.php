<?php

use App\Enums\QuarantineAnomalies;

return [
    'labels' => [
        'Quarantine' => '隔离信息',
        'quarantine' => '隔离信息',
    ],
    'fields' => [
        'uid' => '用户',
        'adminUser' => [
            'name' => '姓名',
            'mobile' => '手机号',
            'dormitory_floor' => '宿舍楼',
            'doorplate' => '宿舍门牌号',
            'classes' => [
                'class_name' => '班级名称'
            ]
        ],
        'admin_user' => [
            'name' => '姓名',
            'mobile' => '手机号',
            'dormitory_floor' => '宿舍楼',
            'doorplate' => '宿舍门牌号',
            'classes' => [
                'class_name' => '班级名称'
            ]
        ],
        'quarantine_reson' => '隔离原因',
        'anomalies' => '隔离情况',
        'start_quarantine' => '开始隔离日期',
        'end_quarantine' => '结束隔离日期',
        'quarantine_days' => '隔离天数',
        'remainder_days' => '剩余天数',
        'remark' => '备注',
    ],
    'options' => [
        'anomalies' => QuarantineAnomalies::asSelectArray()
    ],
];
