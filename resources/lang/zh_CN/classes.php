<?php
return [
    'labels' => [
        'Classes' => '班级管理',
        'class' => '班级管理',
    ],
    'fields' => [
        'faculties' => '院系',
        'class_name' => '班级名称',
        'headteacher' => '班主任',
        'teacher' => [
            'name' => '班主任'
        ],
    ],
    'options' => [
    ],
];
