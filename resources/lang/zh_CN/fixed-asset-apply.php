<?php

use App\Enums\FixedAssetApplyState;

return [
    'labels' => [
        'FixedAssetApply' => '物资申请',
        'fixed-asset' => '物资',
        'apply' => '申请',
    ],
    'fields' => [
        'uid' => '申请人',
        'asset_id' => '申请物资',
        'number' => '申请数量',
        'state' => '状态',
        'opt_uid' => '审核人',
        'opt_time' => '审核时间',
        'apply_time' => '申请时间',
        'adminUser' => [
            'name' => '申请人'
        ],
        'fixedAsset' => [
            'asset' => '申请物资'
        ],
        'admin_user' => [
            'name' => '申请人'
        ],
        'fixed_asset' => [
            'asset' => '申请物资'
        ]
    ],
    'options' => [
        'state' => FixedAssetApplyState::asSelectArray()
    ],
];
