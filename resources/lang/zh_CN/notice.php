<?php
return [
    'labels' => [
        'Notice' => '公告通知',
        'notice' => '公告通知',
    ],
    'fields' => [
        'id' => 'ID',
        'title' => '标题',
        'img_url' => '封面图片',
        'content' => '公告内容',
        'file_url' => '附件',
        'start_time' => '展示开始日期',
        'end_time' => '展示结束日期',
        'admin_id' => '发布人'
    ],
    'options' => []
];
