<?php
return [
    'labels' => [
        'FixedAssetType' => '物资类型',
        'fixed-asset' => '物资',
        'type' => '类型'
    ],
    'fields' => [
        'uid' => '创建人',
        'parent_id' => '上级类型',
        'name' => '名称',
        'order' => '排序',
        'adminUser' => [
            'name' => '创建人'
        ],
        'admin_user' => [
            'name' => '创建人'
        ],
    ],
    'options' => [],
];
