<?php

// @formatter:off
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App\Models{
/**
 * App\Models\AdminUser
 *
 * @property int $id
 * @property string $username
 * @property string $password
 * @property string $name
 * @property string|null $avatar
 * @property int $identity 身份 1:工作人员 2:教师 3:学生
 * @property int $sex 性别 0:未知 1:男 2:女
 * @property string $mobile 手机号
 * @property string|null $class 班级
 * @property int $state 状态 1:正常 2:禁用
 * @property int $age 年龄
 * @property string|null $dormitory_floor 宿舍楼
 * @property string|null $doorplate 宿舍门牌号
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Classes|null $classes
 * @property-read \Illuminate\Database\Eloquent\Collection|\Dcat\Admin\Models\Role[] $roles
 * @property-read int|null $roles_count
 * @method static \Illuminate\Database\Eloquent\Builder|AdminUser newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AdminUser newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AdminUser query()
 * @method static \Illuminate\Database\Eloquent\Builder|AdminUser whereAge($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AdminUser whereAvatar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AdminUser whereClass($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AdminUser whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AdminUser whereDoorplate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AdminUser whereDormitoryFloor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AdminUser whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AdminUser whereIdentity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AdminUser whereMobile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AdminUser whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AdminUser wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AdminUser whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AdminUser whereSex($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AdminUser whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AdminUser whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AdminUser whereUsername($value)
 */
	class AdminUser extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Classes
 *
 * @property int $id
 * @property string $faculties
 * @property string $class_name
 * @property int $headteacher
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\AdminUser|null $teacher
 * @method static \Illuminate\Database\Eloquent\Builder|Classes newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Classes newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Classes query()
 * @method static \Illuminate\Database\Eloquent\Builder|Classes whereClassName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Classes whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Classes whereFaculties($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Classes whereHeadteacher($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Classes whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Classes whereUpdatedAt($value)
 */
	class Classes extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\FixedAsset
 *
 * @property int $id
 * @property int $uid 创建人
 * @property string|null $asset_number 物资编号
 * @property string $asset_name 物资名称
 * @property string $asset_spec 物资规格
 * @property int $asset_type 物资分类
 * @property string $price 价格
 * @property string $inventory_date 日期
 * @property int $amount 总数量
 * @property int $amount_idle 闲置数量
 * @property int $amount_using 在用数量
 * @property string $explain 说明
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property-read \App\Models\AdminUser|null $adminUser
 * @property-read \App\Models\FixedAssetType|null $fixedAssetType
 * @method static \Illuminate\Database\Eloquent\Builder|FixedAsset newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|FixedAsset newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|FixedAsset query()
 * @method static \Illuminate\Database\Eloquent\Builder|FixedAsset whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FixedAsset whereAmountIdle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FixedAsset whereAmountUsing($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FixedAsset whereAssetName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FixedAsset whereAssetNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FixedAsset whereAssetSpec($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FixedAsset whereAssetType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FixedAsset whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FixedAsset whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FixedAsset whereExplain($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FixedAsset whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FixedAsset whereInventoryDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FixedAsset wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FixedAsset whereUid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FixedAsset whereUpdatedAt($value)
 */
	class FixedAsset extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\FixedAssetApply
 *
 * @property int $id
 * @property int $uid 申请人
 * @property int $asset_id 申请物资
 * @property int $number 申请数量
 * @property int $state 状态 1:未审核 2:已通过 3:已拒绝
 * @property string $apply_time
 * @property int|null $opt_uid 审核人
 * @property string|null $opt_time 审核时间
 * @property string $opt_remark 审核备注
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \App\Models\AdminUser|null $adminUser
 * @property-read \App\Models\FixedAsset|null $fixedAsset
 * @method static \Illuminate\Database\Eloquent\Builder|FixedAssetApply newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|FixedAssetApply newQuery()
 * @method static \Illuminate\Database\Query\Builder|FixedAssetApply onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|FixedAssetApply query()
 * @method static \Illuminate\Database\Eloquent\Builder|FixedAssetApply whereApplyTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FixedAssetApply whereAssetId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FixedAssetApply whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FixedAssetApply whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FixedAssetApply whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FixedAssetApply whereNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FixedAssetApply whereOptRemark($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FixedAssetApply whereOptTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FixedAssetApply whereOptUid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FixedAssetApply whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FixedAssetApply whereUid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FixedAssetApply whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|FixedAssetApply withTrashed()
 * @method static \Illuminate\Database\Query\Builder|FixedAssetApply withoutTrashed()
 */
	class FixedAssetApply extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\FixedAssetType
 *
 * @property int $id
 * @property int $uid 创建人
 * @property int $parent_id 上级
 * @property string $name 名称
 * @property int $order 排序
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property-read \App\Models\AdminUser|null $adminUser
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\FixedAsset[] $fixedAsset
 * @property-read int|null $fixed_asset_count
 * @method static \Illuminate\Database\Eloquent\Builder|FixedAssetType newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|FixedAssetType newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|FixedAssetType query()
 * @method static \Illuminate\Database\Eloquent\Builder|FixedAssetType whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FixedAssetType whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FixedAssetType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FixedAssetType whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FixedAssetType whereOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FixedAssetType whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FixedAssetType whereUid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FixedAssetType whereUpdatedAt($value)
 */
	class FixedAssetType extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\HealthyClock
 *
 * @property int $id
 * @property int $uid
 * @property string $date 日期
 * @property string $temperature 体温
 * @property int $symptom 症状 1:正常 2:发烧 3:咳嗽 4:乏力
 * @property string $other 其他状况说明
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\AdminUser|null $adminUser
 * @method static \Illuminate\Database\Eloquent\Builder|HealthyClock newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|HealthyClock newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|HealthyClock query()
 * @method static \Illuminate\Database\Eloquent\Builder|HealthyClock whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|HealthyClock whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|HealthyClock whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|HealthyClock whereOther($value)
 * @method static \Illuminate\Database\Eloquent\Builder|HealthyClock whereSymptom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|HealthyClock whereTemperature($value)
 * @method static \Illuminate\Database\Eloquent\Builder|HealthyClock whereUid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|HealthyClock whereUpdatedAt($value)
 */
	class HealthyClock extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Notice
 *
 * @property int $id
 * @property string $title 标题
 * @property string|null $img_url 封面图片
 * @property string $content 公告内容
 * @property string|null $file_url 附件
 * @property string|null $start_time 展示开始日期
 * @property string|null $end_time 展示结束日期
 * @property int $admin_id 创建人
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property-read \App\Models\AdminUser|null $admin_user
 * @method static \Illuminate\Database\Eloquent\Builder|Notice newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Notice newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Notice query()
 * @method static \Illuminate\Database\Eloquent\Builder|Notice whereAdminId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Notice whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Notice whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Notice whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Notice whereEndTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Notice whereFileUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Notice whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Notice whereImgUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Notice whereStartTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Notice whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Notice whereUpdatedAt($value)
 */
	class Notice extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Quarantine
 *
 * @property int $id
 * @property int $uid 用户
 * @property string $quarantine_reson 隔离原因
 * @property int $anomalies 隔离情况 1:健康 2:疑似 3:确诊
 * @property string $start_quarantine 开始隔离日期
 * @property string $end_quarantine 结束隔离日期
 * @property int $quarantine_days 隔离天数
 * @property string|null $remark 备注
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \App\Models\AdminUser|null $adminUser
 * @method static \Illuminate\Database\Eloquent\Builder|Quarantine newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Quarantine newQuery()
 * @method static \Illuminate\Database\Query\Builder|Quarantine onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Quarantine query()
 * @method static \Illuminate\Database\Eloquent\Builder|Quarantine whereAnomalies($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Quarantine whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Quarantine whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Quarantine whereEndQuarantine($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Quarantine whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Quarantine whereQuarantineDays($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Quarantine whereQuarantineReson($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Quarantine whereRemark($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Quarantine whereStartQuarantine($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Quarantine whereUid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Quarantine whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|Quarantine withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Quarantine withoutTrashed()
 */
	class Quarantine extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\User
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property \Illuminate\Support\Carbon|null $email_verified_at
 * @property string $password
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Sanctum\PersonalAccessToken[] $tokens
 * @property-read int|null $tokens_count
 * @method static \Database\Factories\UserFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User query()
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereUpdatedAt($value)
 */
	class User extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Vaccine
 *
 * @property int $id
 * @property int $uid 员工
 * @property int $number 接种针数
 * @property string|null $first_at 第一针接种日期
 * @property string|null $second_at 第二针接种日期
 * @property string|null $third_at 第三针接种日期
 * @property string|null $reason 未接种原因
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \App\Models\AdminUser|null $adminUser
 * @method static \Illuminate\Database\Eloquent\Builder|Vaccine newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Vaccine newQuery()
 * @method static \Illuminate\Database\Query\Builder|Vaccine onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Vaccine query()
 * @method static \Illuminate\Database\Eloquent\Builder|Vaccine whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vaccine whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vaccine whereFirstAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vaccine whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vaccine whereNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vaccine whereReason($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vaccine whereSecondAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vaccine whereThirdAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vaccine whereUid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vaccine whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|Vaccine withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Vaccine withoutTrashed()
 */
	class Vaccine extends \Eloquent {}
}

